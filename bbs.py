#!/usr/local/bin/python2.7

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#  
# Copyright (C) 2013 Hououin Redflag <shikieiki@yamaxanadu.org>
#  
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#  
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#  
# 0. You just DO WHAT THE FUCK YOU WANT TO

import web
import urlparse
import hashlib
import HTMLParser

from markdown import markdown
from datetime import datetime
from string import Template
from cgi import escape

import config


urls = (
    '/','index',
    '/catalogs/(.*)/','catalogs',
    '/showcatalog/','catalogs',
    '/p/(.*)','view',
    '/action/post','post',
    '/action/reply','reply'
)

db = web.database(dbn='mysql',db=config.database_name,user=config.database_user,pw=config.database_pw)

def ContentProccessor(content):
    return markdown(content.decode('utf-8'),extensions=['codehilite(linenums=True)'])

def GetGravatar(email):
    email = email.strip().lower()
    hash = hashlib.md5(email).hexdigest()
    return 'http://www.gravatar.com/avatar/'+hash

def page404():
    with open('404.html') as handler:
        page = handler.read()

    return web.notfound(page)

class catalogs:
    def gen_page(self,catalog):
        result = db.select('post',where=('catalog="%s"'%catalog),order='datetime')

        htmlfile = open('catalog.html')
        html = Template(htmlfile.read().decode('utf-8'))
        htmlfile.close()

        posts = ''
        
        for post in result:
            post_title = ('<li><a style="font-size:2em" href="/p/%d">%s</a></li>'%(post['pid'],
                                                                                  post['title']))
            posts+=post_title

        return html.substitute(catalog=catalog.decode('utf-8'),posts=posts)

    def GET(self,catalog):
        return self.gen_page(catalog)

    def POST(self):
        catalog = dict(urlparse.parse_qsl(web.data()))['catalog']
        return self.gen_page(catalog)
        
class post:
    def POST(self):
        # Get post data
        data = web.data()
        data = urlparse.parse_qsl(data)
        data = dict(data)

        web.setcookie('author',data['author'].decode('utf-8'))
        web.setcookie('email',data['email'].decode('utf-8'))
        
        #Set time zone
        dt = datetime.now()

        #Escape
        data['author'] = escape(data['author'])
        data['title'] = escape(data['title'])
        data['catalog'] = escape(data['catalog'])

        pid = db.insert('post',
                        author = data['author'].decode('utf-8'),
                        title = data['title'].decode('utf-8'),
                        catalog = data['catalog'].decode('utf-8'),
                        email = data['email'].decode('utf-8'),
                        datetime = dt)

        db.insert('reply',
                  author = data['author'].decode('utf-8'),
                  content = ContentProccessor(data['content']),
                  email = data['email'].decode('utf-8'),
                  datetime = dt,
                 parent = pid)
        
        return '<html><head><meta http-equiv="refresh" content="0.1;url=%s"></head></html>'%(config.website_url + '/p/' + str(pid))

class view:
    def GET(self,name):
        try:
            post = list(db.select('post',where=('pid=%s'%name)))[0]
        except IndexError:
            raise web.notfound()
        
        with open('post_frame.html') as handler:
            post_frame = Template(handler.read().decode('utf-8'))

        with open('post_container.html') as handler:
            post_container = Template(handler.read().decode('utf-8'))

        replys = db.select('reply',where=('parent=%s'%name),order='datetime')
        containers = ''
        
        for reply in replys:
            containers +=post_container.substitute(
                reply_id=reply['id'],
                avatar = GetGravatar(reply['email']),
                author = reply['author'],
                reply = reply['content'],
                datetime = reply['datetime']
            )
            
        return post_frame.substitute(
            title=post['title'],
            datetime=str(post['datetime']),
            author=post['author'],
            posts=containers,
            pid=name,
            catalog=post['catalog']
        )

class reply:
    def POST(self):
        # Get post data
        data = web.data()
        data = urlparse.parse_qsl(data)
        data = dict(data)

        web.setcookie('author',data['author'].decode('utf-8'))
        web.setcookie('email',data['email'].decode('utf-8'))
        
        #Escape
        data['author'] = escape(data['author'])
        data['email'] = escape(data['email'])
        
        db.insert('reply',
                  author = data['author'],
                  content = ContentProccessor(data['content']),
                  email = data['email'],
                  datetime = datetime.now(),
                  parent = data['parent'])

        return '<html><head><meta http-equiv="refresh" content="0.1;url=%s"></head></html>'%(
            config.website_url + 'p/' + data['parent'])
        
class index:
    def GET(self):
        htmlfile = open('index.html')
        html = htmlfile.read().decode('utf-8')
        htmlfile.close()
        
        result = db.select('post',order='datetime')

        posts = ''
        
        for post in result:
            post_title = ('<li><a style="font-size:2em" href="p/%d"><small style="color:black;">%s:</small>%s</a></li>'%(
                post['pid'],post['author'],post['title']))
            
            posts=post_title+posts

        return html.replace('{{ PostList }}',posts)

if __name__ == '__main__':
    app = web.application(urls,globals())
    app.notfound = page404
    web.wsgi.runwsgi = lambda func, addr=None: web.wsgi.runfcgi(func, addr)
    app.run()
